package com.trainingmicro.frontend.frontend.controller;

import com.trainingmicro.frontend.frontend.service.PromoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class ProductController {

    @Autowired private PromoService promoService;

    @GetMapping("/product/list")
    public ModelMap daftarProduct(){
        return new ModelMap()
                .addAttribute("dataProduk", promoService.dataSemuaProduk());
    }

    @GetMapping("/backend")
    public ModelMap backendInfo() {
        return new ModelMap()
                .addAttribute("hostInfo",
                        promoService.backendInfo());
    }
}
