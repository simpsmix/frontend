package com.trainingmicro.frontend.frontend.service;

import com.trainingmicro.frontend.frontend.dto.Product;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.cloud.openfeign.FeignClient;

import java.util.Map;

@FeignClient(name="promo", fallback = PromoServiceFallback.class)
public interface PromoService {

    @GetMapping("/product/")
    Iterable<Product> dataSemuaProduk();

    @GetMapping("/hostinfo")
    public Map<String, Object> backendInfo();
}
